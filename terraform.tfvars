region = "us-central1"
region_zone = "us-central1-a"

# Project graceful-matter-161422
# project_name = "graceful-matter-161422"
# home_directory = "/home/terry"
# account_file_path = "~/gcp_credentials.json"
# gce_ssh_pub_key_file = "~/.ssh/id_rsa.pub"
# gce_ssh_private_key_file = "~/.ssh/id_rsa"
# gce_ssh_user = "terry"

# Project vodafone-cassandra-poc
project_name = "vodafone-cassandra-poc"
home_directory = "/home/dhariwal"
account_file_path = "~/Vodafone-Cassandra-PoC.json"
gce_ssh_pub_key_file = "~/.ssh/id_rsa.pub"
gce_ssh_private_key_file = "~/.ssh/id_rsa"
gce_ssh_user = "dhariwal"
