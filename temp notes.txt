Create Project
Create Service Account Key for “Compute Engine default service account” in IAM (download JSON)
Upload JSON onto console VM
Create ssh keys for the console w/o prompts (https://stackoverflow.com/questions/43235179/how-execute-ssh-keygen-without-prompt)
  yes y | ssh-keygen -q -t rsa -N '' >/dev/null
  mv y.pub ~/.ssh/id_rsa.pub
  mv y ~/.ssh/id_rsa
Under Compute Engine, under Metadata add a key called ssh-keys and set the value of cat ida_rsa.pub (watch out for funny spacing when you paste)
Download Terraform 
  wget https://releases.hashicorp.com/terraform/0.10.6/terraform_0.10.6_linux_amd64.zip
Unzip Terraform
  unzip terraform_0.10.6_linux_amd64.zip -d .
Clone vodaforn-iac-and-autiomation
  git clone https://bitbucket.org/cleverbitsio/vodafone-iac-and-automation.git
Initialise the cloned project using terraform
  cd vodafone-iac-and-automation/
  ../terraform init
Update terraform.tfvars and variables.tf and provisioner-scripts/puppet-agent-provisioners-1.sh
Run terraform using the new key
  ../terraform plan -var "account_file_path=~/Vodafone-Cassandra-PoC-cf0b8dce8bc7.json"
  ../terraform apply -var "account_file_path=~/Vodafone-Cassandra-PoC-cf0b8dce8bc7.json" -var "gce_ssh_user=dhariwal" -var "home_directory=/home/dhariwal"


——

sudo /opt/puppetlabs/bin/puppet cert sign node.c.vodafone-cassandra-poc.internal; \
sudo /opt/puppetlabs/bin/puppet cert sign node2.c.vodafone-cassandra-poc.internal; \
sudo /opt/puppetlabs/bin/puppet cert sign node3.c.vodafone-cassandra-poc.internal; \
sudo /opt/puppetlabs/bin/puppet cert sign spark.c.vodafone-cassandra-poc.internal; \
sudo /opt/puppetlabs/bin/puppet cert sign spark2.c.vodafone-cassandra-poc.internal; \
sudo /opt/puppetlabs/bin/puppet cert sign spark3.c.vodafone-cassandra-poc.internal; 

sudo /opt/puppetlabs/bin/puppet agent --test --server=master.c.vodafone-cassandra-poc.internal

sudo su - cassandra -c "/opt/apache-cassandra/bin/cassandra"

watch -n 2 /opt/apache-cassandra/bin/nodetool -p 7199 status

—

git clone https://cleverbitsio@bitbucket.org/cleverbitsio/vodafone-source-data-load.git
cd vodafone-source-data-load/
/opt/apache-cassandra/bin/cqlsh node.c.vodafone-cassandra-poc.internal
SOURCE 'CQL-commands.sql'

cd ~/vodafone-source-data-load
sudo /opt/apache-spark/bin/spark-submit --class "com.cleverbits.io.spark_examples.Vodafone" --master local target/spark-examples-0.0.1-SNAPSHOT.jar -n "node.c.vodafone-cassandra-poc.internal,node2.c.vodafone-cassandra-poc.internal"


——
Start ALL APP Servers
//you must be in the same dir for it to pick up cert
cd ~/vodafone-client-rest 
java -cp ~/vodafone-client-rest/target/cassandra-client-rest-service-0.1.0.jar CassandraClientREST.Application &
java -jar ~/vodafone-client-web-interface/target/spring-boot-web-jsp-1.0.war &

watch -n 1 "/opt/apache-cassandra/bin/cqlsh node.c.vodafone-cassandra-poc.internal -e 'select count(*) from vodafone.load'"


---
Costs of proper test:
https://cloud.google.com/products/calculator/#id=c3917ddf-8e11-43eb-ae21-38e8e67dc514

---
need to automate setting the java heap size for Cassandra:
https://stackoverflow.com/questions/34745419/how-to-set-cassandra-2-0-jvm-heap-size-of-8gb
cassandra/conf/jvm.options

